<div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
   <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
      <a href="#" class="top_menu_toggle"><i class="material-icons md-24">&#xE8F0;</i></a>
      <div class="uk-dropdown uk-dropdown-width-2">
         <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
            <div class="uk-width-3-3">
               <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                  @if($user->checkAksesModule('finance'))
                  <a href="{{ route('backDashboardFinance') }}">
                     <i class="material-icons md-36">&#xE85C;</i>
                     <span class="uk-text-muted uk-display-block">Finance</span>
                  </a>
                  @endif

                  @if($user->checkAksesModule('hr'))
                  <a href="{{ route('backDashboardHr') }}">
                     <i class="material-icons md-36">&#xE87C;</i>
                     <span class="uk-text-muted uk-display-block">HR</span>
                  </a>
                  @endif

                  @if($user->checkAksesModule('master'))
                  <a href="{{route('backDashboardMaster')}}">
                     <i class="material-icons md-36">&#xE869;</i>
                     <span class="uk-text-muted uk-display-block">CONFIG</span>
                  </a>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</div>