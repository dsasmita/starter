
@if($position['module'] == 'hr')
<li class="{{$position['module'] == 'hr' && $position['menu'] == 'dashboard' ? 'current_section' : ''}}" title="Dashboard MIS">
   <a href="{{route('backDashboardHr')}}">
   <span class="menu_icon"><i class="material-icons">store_mall_directory</i></span>
   <span class="menu_title">Module Hr</span>
   </a>
</li>
<li>
   <a href="#">
   <span class="menu_icon"><i class="material-icons">&#xE85C;</i></span>
   <span class="menu_title">Master Employee</span>
   </a>
   <ul>
      <li><a href="javascript:void(0)">Master AOP</a></li>
      <li><a href="javascript:void(0)">Master RBB</a></li>
   </ul>
</li>
<li>
   <a href="#">
   <span class="menu_icon"><i class="material-icons">&#xE53E;</i></span>
   <span class="menu_title">Payroll</span>
   </a>
   <ul>
      <li><a href="javascript:void(0)">Financial Highlight (Daily)</a></li>
      <li><a href="javascript:void(0)">Financial Highlight (Monthly)</a></li>
      <li><a href="javascript:void(0)">Rasio Report</a></li>
      <li><a href="javascript:void(0)">Report Daily</a></li>
   </ul>
</li>
@endif