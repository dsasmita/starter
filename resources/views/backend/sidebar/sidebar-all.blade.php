<li class="{{ $position['module'] == 'dashboard' ? 'current_section' : ''}}" title="Dashboard">
   <a href="{{route('backDashboard')}}">
   <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
   <span class="menu_title">Dashboard</span>
   </a>
</li>
@if($position['module'] == 'dashboard')
   @if($user->checkAksesModule('master'))
   <li class="{{$position['module'] == 'master' && $position['menu'] == 'dashboard' ? 'current_section' : ''}}" title="Dashboard Master">
      <a href="{{route('backDashboardMaster')}}">
      <span class="menu_icon"><i class="material-icons">build</i></span>
      <span class="menu_title">Module Config</span>
      </a>
   </li>
   @endif

   @if($user->checkAksesModule('finance'))
   <li class="{{$position['module'] == 'mis' && $position['menu'] == 'dashboard' ? 'current_section' : ''}}" title="Dashboard MIS">
      <a href="{{route('backDashboardFinance')}}">
      <span class="menu_icon"><i class="material-icons">&#xE85C;</i></span>
      <span class="menu_title">Module Finance</span>
      </a>
   </li>
   @endif

   @if($user->checkAksesModule('hr'))
   <li class="{{$position['module'] == 'hr' && $position['menu'] == 'dashboard' ? 'current_section' : ''}}" title="Dashboard MIS">
      <a href="{{route('backDashboardHr')}}">
      <span class="menu_icon"><i class="material-icons">&#xE87C;</i></span>
      <span class="menu_title">Module HR</span>
      </a>
   </li>
   @endif
@else
	<!-- sidebar finance -->
	@include('backend/sidebar/sidebar-finance')
	<!-- sidebar setting -->
	@include('backend/sidebar/sidebar-master')
   <!-- sidebar HR -->
   @include('backend/sidebar/sidebar-hr')

@endif