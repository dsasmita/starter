@if($position['module'] == 'master')
   <li class="{{$position['module'] == 'master' && $position['menu'] == 'dashboard' ? 'current_section' : ''}}" title="Dashboard">
      <a href="{{route('backDashboardMaster')}}">
      <span class="menu_icon"><i class="material-icons">store_mall_directory</i></span>
      <span class="menu_title">Dashboard Master</span>
      </a>
   </li>
   @if($user->chechAksesGroupModule('master','users'))
   <li class="{{$position['module'] == 'master' && $position['menu'] == 'user' ? 'current_section' : ''}}" title="Master Users">
      <a href="{{route('backMasterUsers')}}">
         <span class="menu_icon"><i class="material-icons">&#xE0BA;</i></span>
         <span class="menu_title">Master Users</span>
      </a>
   </li>
   @endif

   @if($user->chechAksesGroupModule('master','roles'))
   <li class="{{$position['module'] == 'master' && $position['menu'] == 'roles' ? 'current_section' : ''}}" title="Master Roles">
      <a href="{{route('backMasterRoles')}}">
         <span class="menu_icon"><i class="material-icons">&#xE429;</i></span>
         <span class="menu_title">Master Roles</span>
      </a>
   </li>
   @endif

   @if($user->chechAksesGroupModule('master','rules'))
   <li class="{{$position['module'] == 'master' && $position['menu'] == 'rules' ? 'current_section' : ''}}" title="Master Rules">
      <a href="{{route('backMasterRules')}}">
         <span class="menu_icon"><i class="material-icons">vpn_lock</i></span>
         <span class="menu_title">Master Rules</span>
      </a>
   </li>
   @endif

   @if($user->chechAksesGroupModule('master','setting'))
   <!--
   <li class="{{$position['module'] == 'master' && $position['menu'] == 'setting' ? 'current_section' : ''}}" title="Setting">
      <a href="{{route('backMasterSettings')}}">
         <span class="menu_icon"><i class="material-icons">build</i></span>
         <span class="menu_title">Setting</span>
      </a>
   </li>
   -->
   <li>
     <a href="#">
         <span class="menu_icon"><i class="material-icons">build</i></span>
         <span class="menu_title">Setting</span>
     </a>
     <ul>
         <li class="{{$position['module'] == 'master' && $position['menu'] == 'setting' ? 'act_item' : ''}}">
            <a href="{{route('backMasterSettings')}}">General Setting</a></li>
         <li class="{{$position['module'] == 'master' && $position['menu'] == 'setting_hris' ? 'act_item' : ''}}">
            <a href="{{route('backMasterSettings')}}">HRIS Setting</a></li>
         <!--<li class="menu_subtitle">WYSIWYG Editors</li>-->
     </ul>
 </li>
   @endif
@endif