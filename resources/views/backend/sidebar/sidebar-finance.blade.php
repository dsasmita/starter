@if($position['module'] == 'finance')
<li class="{{$position['module'] == 'finance' && $position['menu'] == 'dashboard' ? 'current_section' : ''}}" title="Dashboard MIS">
   <a href="{{route('backDashboardFinance')}}">
   <span class="menu_icon"><i class="material-icons">store_mall_directory</i></span>
   <span class="menu_title">Module Finance</span>
   </a>
</li>
<li>
   <a href="#">
   <span class="menu_icon"><i class="material-icons">&#xE85C;</i></span>
   <span class="menu_title">Master Planning</span>
   </a>
   <ul>
      <li><a href="javascript:void(0)">Master AOP</a></li>
      <li><a href="javascript:void(0)">Master RBB</a></li>
   </ul>
</li>
<li>
   <a href="#">
   <span class="menu_icon"><i class="material-icons">&#xE53E;</i></span>
   <span class="menu_title">Financial</span>
   </a>
   <ul>
      <li><a href="javascript:void(0)">Financial Highlight (Daily)</a></li>
      <li><a href="javascript:void(0)">Financial Highlight (Monthly)</a></li>
      <li><a href="javascript:void(0)">Rasio Report</a></li>
      <li><a href="javascript:void(0)">Report Daily</a></li>
   </ul>
</li>
@endif