@extends('backend/template')

@section('title', $title)

@section('css')
@stop

@section('content')
<!-- BEGIN PAGE HEADER-->
<div id="page_content">
    <div id="page_content_inner">
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
            <div class="uk-width-large-7-10">
                <div class="md-card">
                    <div class="user_heading">
                        <div class="user_heading_menu" data-uk-dropdown="{pos:'left-top'}">
                            <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                            <div class="uk-dropdown uk-dropdown-small">
                                <ul class="uk-nav">
                                    <li><a href="#">Action 1</a></li>
                                    <li><a href="#">Action 2</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="user_heading_avatar">
                            @if($user->avatar != '')
                            <img src="{{asset('')}}/{{$user->avatar}}" class="" alt="avatar">
                            @else
                            <img src="{{asset('assets')}}/assets/img/avatars/avatar_11.png" class="" alt="user avatar">
                            @endif
                        </div>
                        <div class="user_heading_content">
                            <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">Dana Gorczany</span><span class="sub-heading">Land acquisition specialist</span></h2>
                            <ul class="user_stats">
                                <li>
                                    <h4 class="heading_a">2391 <span class="sub-heading">Posts</span></h4>
                                </li>
                                <li>
                                    <h4 class="heading_a">120 <span class="sub-heading">Photos</span></h4>
                                </li>
                                <li>
                                    <h4 class="heading_a">284 <span class="sub-heading">Following</span></h4>
                                </li>
                            </ul>
                        </div>
                        <a class="md-fab md-fab-small md-fab-accent" href="{{route('backProfil')}}">
                            <i class="material-icons">&#xE150;</i>
                        </a>
                    </div>
                    <div class="user_content">
                        <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                            <li class="uk-active"><a href="#">About</a></li>
                            <li><a href="#">Photos</a></li>
                            <li><a href="#">Posts</a></li>
                        </ul>
                        <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                            <li>
                                Temporibus tempora qui laudantium non non et nam quos rerum in cum cumque aliquam possimus aliquam temporibus autem esse sint tempora tempora qui dignissimos alias quibusdam corporis doloremque alias veritatis magnam eum est accusantium voluptatem nisi maxime laborum vel quam ad alias ex doloremque quidem laborum impedit sed rerum architecto non et dolorum et cumque autem qui deleniti sint doloribus tenetur quo ullam culpa sit quo corrupti autem accusantium et ipsum perspiciatis dolor dolor illo eaque voluptatem omnis aut non consequatur tempore placeat laborum voluptatibus et quam tempore rerum pariatur adipisci vel fugit quidem nihil distinctio officia at quia sed minima quasi deleniti ad et deserunt ducimus nihil quia voluptatem reiciendis sequi ducimus asperiores unde eum sint suscipit molestiae aspernatur est ut occaecati praesentium ut voluptas eos molestias rerum officiis tempore quia repellendus saepe quae labore odit repudiandae et aut ad quos labore incidunt nobis deserunt amet placeat sit sequi officiis corrupti debitis velit dolores sit omnis aut voluptatum.                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                    <div class="uk-width-large-1-2">
                                        <h4 class="heading_c uk-margin-small-bottom">Contact Info</h4>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">herman.farrell@ritchie.com</span>
                                                    <span class="uk-text-small uk-text-muted">Email</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">(087)512-0476x05298</span>
                                                    <span class="uk-text-small uk-text-muted">Phone</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon uk-icon-facebook-official"></i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">facebook.com/envato</span>
                                                    <span class="uk-text-small uk-text-muted">Facebook</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon uk-icon-twitter"></i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">twitter.com/envato</span>
                                                    <span class="uk-text-small uk-text-muted">Twitter</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="uk-width-large-1-2">
                                        <h4 class="heading_c uk-margin-small-bottom">My groups</h4>
                                        <ul class="md-list">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Cloud Computing</a></span>
                                                    <span class="uk-text-small uk-text-muted">30 Members</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Account Manager Group</a></span>
                                                    <span class="uk-text-small uk-text-muted">163 Members</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">Digital Marketing</a></span>
                                                    <span class="uk-text-small uk-text-muted">158 Members</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="#">HR Professionals Association - Human Resources</a></span>
                                                    <span class="uk-text-small uk-text-muted">277 Members</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <h4 class="heading_c uk-margin-bottom">Timeline</h4>
                                <div class="timeline">
                                    <div class="timeline_item">
                                        <div class="timeline_icon timeline_icon_success"><i class="material-icons">&#xE85D;</i></div>
                                        <div class="timeline_date">
                                            09 <span>Nov</span>
                                        </div>
                                        <div class="timeline_content">Created ticket <a href="#"><strong>#3289</strong></a></div>
                                    </div>
                                    <div class="timeline_item">
                                        <div class="timeline_icon timeline_icon_danger"><i class="material-icons">&#xE5CD;</i></div>
                                        <div class="timeline_date">
                                            15 <span>Nov</span>
                                        </div>
                                        <div class="timeline_content">Deleted post <a href="#"><strong>Culpa consectetur culpa illum ad voluptatem quia.</strong></a></div>
                                    </div>
                                    <div class="timeline_item">
                                        <div class="timeline_icon"><i class="material-icons">&#xE410;</i></div>
                                        <div class="timeline_date">
                                            19 <span>Nov</span>
                                        </div>
                                        <div class="timeline_content">
                                            Added photo
                                            <div class="timeline_content_addon">
                                                <img src="{{asset('assets')}}/assets/img/gallery/Image16.jpg" alt=""/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="timeline_item">
                                        <div class="timeline_icon timeline_icon_primary"><i class="material-icons">&#xE0B9;</i></div>
                                        <div class="timeline_date">
                                            21 <span>Nov</span>
                                        </div>
                                        <div class="timeline_content">
                                            New comment on post <a href="#"><strong>Qui facere id.</strong></a>
                                            <div class="timeline_content_addon">
                                                <blockquote>
                                                    Magnam distinctio possimus aut quae dolores vitae ut minima ea voluptate debitis rerum placeat earum rerum dignissimos nam voluptas labore possimus harum.&hellip;
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="timeline_item">
                                        <div class="timeline_icon timeline_icon_warning"><i class="material-icons">&#xE7FE;</i></div>
                                        <div class="timeline_date">
                                            29 <span>Nov</span>
                                        </div>
                                        <div class="timeline_content">
                                            Added to Friends
                                            <div class="timeline_content_addon">
                                                <ul class="md-list md-list-addon">
                                                    <li>
                                                        <div class="md-list-addon-element">
                                                            <img class="md-user-image md-list-addon-avatar" src="{{asset('assets')}}/assets/img/avatars/avatar_02_tn.png" alt=""/>
                                                        </div>
                                                        <div class="md-list-content">
                                                            <span class="md-list-heading">Rhoda Oberbrunner</span>
                                                            <span class="uk-text-small uk-text-muted">Laboriosam amet aliquid blanditiis ut.</span>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div id="user_profile_gallery" data-uk-check-display class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4" data-uk-grid="{gutter: 4}">
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image01.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image01.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image02.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image02.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image03.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image03.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image04.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image04.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image05.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image05.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image06.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image06.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image07.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image07.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image08.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image08.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image09.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image09.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image10.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image10.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image11.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image11.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image12.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image12.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image13.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image13.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image14.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image14.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image15.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image15.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image16.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image16.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image17.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image17.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image18.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image18.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image19.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image19.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image20.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image20.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image21.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image21.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image22.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image22.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image23.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image23.jpg" alt=""/>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="{{asset('assets')}}/assets/img/gallery/Image24.jpg" data-uk-lightbox="{group:'user-photos'}">
                                            <img src="{{asset('assets')}}/assets/img/gallery/Image24.jpg" alt=""/>
                                        </a>
                                    </div>
                                </div>
                                <ul class="uk-pagination uk-margin-large-top">
                                    <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                                    <li class="uk-active"><span>1</span></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><span>&hellip;</span></li>
                                    <li><a href="#">20</a></li>
                                    <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                                </ul>
                            </li>
                            <li>
                                <ul class="md-list">
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Ad assumenda ut harum ut totam tempore.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">09 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">14</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">749</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Inventore qui natus nam illo aut optio.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">19 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">7</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">423</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Maiores nisi mollitia assumenda totam quasi.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">18 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">21</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">525</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Vitae molestiae fugit occaecati sit velit earum autem.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">03 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">18</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">289</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Sint ipsa quia voluptatem itaque dignissimos ducimus.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">10 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">18</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">896</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Atque et quo sit voluptatem dicta.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">10 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">28</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">110</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Et atque eum a aut.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">07 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">11</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">930</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Illum vero modi sed.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">11 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">7</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">322</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Dolore quaerat animi ex itaque excepturi aut itaque.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">22 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">16</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">332</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">A molestiae animi ut sit doloribus id velit.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">24 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">3</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">791</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Voluptatem officia velit voluptas delectus dolores.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">22 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">10</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">327</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Eius qui et incidunt laborum consequuntur.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">14 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">28</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">547</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Quos dignissimos ratione delectus id et autem nobis.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">24 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">16</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">315</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Quam quia culpa quidem.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">09 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">18</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">875</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Distinctio et voluptate aspernatur quis soluta ut.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">15 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">27</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">537</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Dolore laboriosam aliquid qui dolorem saepe quia.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">12 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">8</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">517</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Ipsa voluptas in sit eligendi aut eum consequuntur.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">12 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">13</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">865</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Eum quasi adipisci a impedit alias distinctio dignissimos iusto.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">10 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">23</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">161</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Sunt ea iusto vel tenetur.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">17 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">15</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">669</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="md-list-content">
                                            <span class="md-list-heading"><a href="#">Voluptatibus voluptate ad eum in aut et.</a></span>
                                            <div class="uk-margin-small-top">
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE192;</i> <span class="uk-text-muted uk-text-small">24 Nov 2015</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE0B9;</i> <span class="uk-text-muted uk-text-small">10</span>
                                            </span>
                                            <span class="uk-margin-right">
                                                <i class="material-icons">&#xE417;</i> <span class="uk-text-muted uk-text-small">754</span>
                                            </span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-3-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-margin-medium-bottom">
                            <h3 class="heading_c uk-margin-bottom">Alerts</h3>
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Aut labore.</span>
                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Et quam consectetur minus dolore accusamus qui.</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Libero voluptas.</span>
                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Odio est quam animi eveniet dolores culpa sint.</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Est enim.</span>
                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Aspernatur repellat cupiditate harum necessitatibus consectetur.</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <h3 class="heading_c uk-margin-bottom">Friends</h3>
                        <ul class="md-list md-list-addon uk-margin-bottom">
                            <li>
                                <div class="md-list-addon-element">
                                    <img class="md-user-image md-list-addon-avatar" src="{{asset('assets')}}/assets/img/avatars/avatar_02_tn.png" alt=""/>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Judd VonRueden</span>
                                    <span class="uk-text-small uk-text-muted">Nulla inventore blanditiis alias et.</span>
                                </div>
                            </li>
                            <li>
                                <div class="md-list-addon-element">
                                    <img class="md-user-image md-list-addon-avatar" src="{{asset('assets')}}/assets/img/avatars/avatar_07_tn.png" alt=""/>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Brando Runolfsson</span>
                                    <span class="uk-text-small uk-text-muted">Sed ut mollitia accusamus.</span>
                                </div>
                            </li>
                            <li>
                                <div class="md-list-addon-element">
                                    <img class="md-user-image md-list-addon-avatar" src="{{asset('assets')}}/assets/img/avatars/avatar_06_tn.png" alt=""/>
                                </div>
                                <div class="md-list-content">
                                    <span class="md-list-heading">Morris Glover</span>
                                    <span class="uk-text-small uk-text-muted">Rem ab labore.</span>
                                </div>
                            </li>
                        </ul>
                        <a class="md-btn md-btn-flat md-btn-flat-primary" href="#">Show all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

@section('javascript')
@stop