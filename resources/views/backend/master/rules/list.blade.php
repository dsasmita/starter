@extends('backend/template')
@section('title', $title)
@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">USERS</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <a href="{{ route('backMasterRulesCreate') }}" class="md-btn md-btn-primary"><i class="material-icons">vpn_lock</i> New Rules</a>
                <div class="uk-overflow-container">
                    <table id="dt_scroll" class="uk-table uk-text-nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Module</th>
                                <th>Group</th>
                                <th>Action</th>
                                <th>Modified By</th>
                                <th>Action</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            ?>
                            @foreach($lists as $data)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ strtoupper($data->module) }}</td>
                                <td>{{ $data->group }}</td>
                                <td>{{ $data->action }}</td>
                                <td>
                                    @if($data->modifiedBy)
                                        {{$data->modifiedBy->username}}<br>
                                    @endif
                                    {{date('F jS \\a\\t g:ia',strtotime($data->updated_at))}}
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <a href="{{ route('backMasterRulesUpdate',$data->id) }}" title="Update" class="btn btn-info"><i class="material-icons md-icon md-24">&#xE254;</i></a>
                                        <a onclick="return confirm('Apakah anda yakin?')" href="{{ route('backMasterRulesDelete',$data->id) }}" title="Delete" class="btn btn-danger"><i class="material-icons md-24 uk-text-danger md-icon">&#xE872;</i></a>                                                                                     
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<script>
    //put all about javascript here
</script>
@stop