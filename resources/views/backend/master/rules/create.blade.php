@extends('backend/template')

@section('title', $title)

@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">USERS</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                @if(Session::get('status-update'))
          <div role="alert" class="alert alert-success">
              <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              <strong>Well done!</strong> {{Session::get('status-update')}}
          </div>
          @endif
          @foreach ($errors->all() as $message)
          <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Warning!</strong> {{$message}}
          </div>
          @endforeach
          <div class="panel-body">
              <form class="form-horizontal" action="{{ route('backMasterRulesCreatePost') }}" method="post">
                  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <h3 class="panel-title"><strong>Master Role</strong> Update</h3>
                      </div>
                      <div class="panel-body">
                          <div class="form-group">
                              <label class="col-md-3 col-xs-12 control-label" for="module">Module</label>
                              <div class="col-md-6 col-xs-12">
                                  <div class="input-group">
                                      <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                      <input type="text" class="form-control" name="module" value="{{old('module')}}" id="module" placeholder="module"/>
                                  </div>
                                  <span class="help-block">module name</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-md-3 col-xs-12 control-label" for="module">Group</label>
                              <div class="col-md-6 col-xs-12">
                                  <div class="input-group">
                                      <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                      <input type="text" class="form-control" name="group" value="{{old('group')}}" id="group" placeholder="group"/>
                                  </div>
                                  <span class="help-block">group name</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-md-3 col-xs-12 control-label" for="action">Action</label>
                              <div class="col-md-6 col-xs-12">
                                  <div class="input-group">
                                      <span class="input-group-addon"><span class="fa fa-asterisk"></span></span>
                                      <input type="text" class="form-control" name="action" value="{{old('action')}}" id="action" placeholder="action"/>
                                  </div>
                                  <span class="help-block">action name</span>
                              </div>
                          </div>

                          <div class="form-group">
                              <label class="col-md-3 col-xs-12 control-label" for="description">Description</label>
                              <div class="col-md-6 col-xs-12">
                                  <textarea class="form-control" id="description" name="description" placeholder="description" rows="3">{{old('description')}}</textarea>
                                  <span class="help-block">description</span>
                              </div>
                          </div>

                      </div>
                      <div class="panel-footer">
                          <button class="btn btn-default" type="reset">Clear Form</button>                                    
                          <button class="btn btn-success pull-right">Submit</button>
                      </div>
                  </div>
              </form> 
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
    //put all about javascript here
</script>
@stop