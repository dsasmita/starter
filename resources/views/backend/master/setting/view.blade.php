@extends('backend/template')
@section('title', $title)

@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">SETTING</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <form class="form-horizontal" action="{{route('backMasterSettingsGeneralPost')}}" method="post">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>General</strong> Setting</h3>
                    </div>
                    <div class="panel-body">
                        @if(Session::get('update-success'))
                        <div id="" class="Metronic-alerts alert alert-success fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <i class="fa-lg fa fa-warning"></i>  
                            {{Session::get('update-success')}}
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" for="site_name">Site Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" class="form-control" required name="site_name" value="{{$setting->getSetting('site_name')}}" id="site_name" placeholder="site name"/>
                                </div>
                                <span class="help-block">input <i>Site name</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" for="setting_db">Setting DB</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" class="form-control" required name="setting_db" value="{{$setting->getSetting('setting_db')}}" id="setting_db" placeholder="setting db"/>
                                </div>
                                <span class="help-block">input <i>DB setting</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" for="admin_path">Prefix Admin Path</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" class="form-control" required name="admin_path" value="{{$setting->getSetting('admin_path')}}" id="admin_path" placeholder="admin path"/>
                                </div>
                                <span class="help-block">input <i>admin path</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" for="site_slogan">Site Slogan</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" class="form-control" required name="site_slogan" value="{{$setting->getSetting('site_slogan')}}" id="site_slogan" placeholder="slogan"/>
                                </div>
                                <span class="help-block">input <i>site slogan</i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label" for="site_records_per_page">Record /page</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                    <input type="text" class="form-control" required name="site_records_per_page" value="{{$setting->getSetting('site_records_per_page')}}" id="site_records_per_page" placeholder="record /page"/>
                                </div>
                                <span class="help-block">input <i>record /page</i></span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-default" type="reset">Clear Form</button>                                    
                        <button class="btn btn-success pull-right">Submit</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<script>
    //put all about javascript here
</script>
@stop