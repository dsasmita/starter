@extends('backend/template')

@section('title', $title)

@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">USERS</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                @foreach ($errors->all() as $message)
                <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Warning!</strong> {{$message}}
                </div>
                @endforeach
                 <form class="form-horizontal" action="{{ route('backMasterUsersCreatePost') }}" method="post">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="panel panel-default">
                       <div class="panel-heading">
                          <h3 class="panel-title"><strong>User</strong> Create</h3>
                       </div>
                       <div class="panel-body">
                          <div class="form-group">
                             <label class="col-md-3 col-xs-12 control-label" for="username">Username</label>
                             <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                   <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                   <input type="text" class="form-control" name="username" value="{{ old('username') }}" id="username" placeholder="username"/>
                                </div>
                                <span class="help-block">input <i>alpha numeric</i></span>
                             </div>
                          </div>
                          <div class="form-group">
                             <label class="col-md-3 col-xs-12 control-label" for="name">Full Name</label>
                             <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                   <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                   <input type="text" name="name" value="{{ old('name') }}" class="form-control" id="name" placeholder="name"/>
                                </div>
                                <span class="help-block">Input full name</span>
                             </div>
                          </div>
                          <div class="form-group">
                             <label class="col-md-3 col-xs-12 control-label" for="nik">NIK</label>
                             <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                   <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                   <input type="text" name="nik" value="{{ old('nik') }}" class="form-control" id="nik" placeholder="nik"/>
                                </div>
                                <span class="help-block">Input NIK User</span>
                             </div>
                          </div>
                          <div class="form-group">
                             <label class="col-md-3 col-xs-12 control-label" for="status">Status User</label>
                             <div class="col-md-6 col-xs-12">
                                <select class="form-control select" name="status" id="status">
                                    <option @if(old('status') == 'Aktif') selected @endif value="Aktif">Active</option>
                                    <option @if(old('status') == 'Non-Aktif') selected @endif value="Non-Aktif">Non-Active</option>
                                </select>
                                <span class="help-block">User access status</span>
                             </div>
                          </div>
                          <div class="form-group">
                             <label class="col-md-3 col-xs-12 control-label" for="role">Role Access</label>
                             <div class="col-md-6 col-xs-12">
                                <select class="form-control select" name="role" id="role">
                                    <option value="">-role access-</option>
                                    @foreach($roles as $role)
                                        <option @if(old('role') == $role->id) selected @endif value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                <span class="help-block">User role access</span>
                             </div>
                          </div>
                          <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label" for="access_ip">Access From IP</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" name="access_ip" value="{{old('access_ip')}}" class="form-control" id="access_ip" placeholder="access from IP"/>
                                    </div>
                                    <span class="help-block">split with ";" for many IP<br> make * to access from all</span>
                                </div>
                            </div>
                       </div>
                       <div class="panel-footer">
                          <button class="btn btn-default" type="reset">Clear Form</button>                                    
                          <button class="btn btn-success pull-right">Submit</button>
                       </div>
                    </div>
                 </form>  
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
    //put all about javascript here
</script>
@stop