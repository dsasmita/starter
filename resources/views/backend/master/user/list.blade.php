@extends('backend/template')
@section('title', $title)

@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">USERS</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <a href="{{ route('backMasterUsersCreate') }}" class="md-btn md-btn-primary"><i class="material-icons md-24">&#xE7FE;</i> New User</a>
                <div class="uk-overflow-container">
                    <table id="dt_scroll" class="uk-table uk-text-nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>NIK</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Last Login</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($lists as $usr)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $usr->nik }}</td>
                                <td>{{ $usr->name }}<br><small>{{ $usr->username }}</small></td>
                                <td>
                                    @if($usr->roles()->first())
                                    {{ $usr->roles()->first()->name }}
                                    @endif
                                </td>
                                <td>{{ $usr->status }}</td>
                                <td>{{ date('d MY H:i',strtotime($usr->last_login)) }}</td>
                                <td>
                                    <center>
                                      <a href="{{ route('backMasterUsersUpdate',$usr->id) }}" title="Update" class=""><i class="material-icons md-icon md-24">&#xE254;</i></a>
                                      @if($status == 'show')
                                      <a href="{{ route('backMasterUsersDelete',$usr->id) }}" onclick="return confirm('Sure?')" title="Delete" class="btn btn-danger"><i class="material-icons md-24 uk-text-danger md-icon">&#xE872;</i></a>                                                                                     
                                      @else
                                      <a href="{{ route('backMasterUsersDelete',$usr->id).'?action=restore' }}" onclick="return confirm('Sure?')" title="restore" class="btn btn-warning"><i class="material-icons md-icon md-24 uk-text-warning">&#xE86A;</i></a>
                                      @endif
                                    </center>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <?php echo $lists->appends(['status' => $status])->render() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
    //put all about javascript here
    $(document).ready(function() {
        $('.pagination').addClass('uk-pagination uk-margin-medium-top uk-margin-medium-bottom');
        $('.pagination .active').addClass('uk-active');
    });
</script>
@stop