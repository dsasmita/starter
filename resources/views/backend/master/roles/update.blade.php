@extends('backend/template')

@section('title', $title)

@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">ROLE</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                @if(Session::get('status-update'))
                <div role="alert" class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <strong>Well done!</strong> {{Session::get('status-create')}}
                </div>
                @endif
                @foreach ($errors->all() as $message)
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Warning!</strong> {{$message}}
                </div>
                @endforeach
                <form class="form-horizontal" action="{{ route('backMasterRolesUpdatePost',$data->id) }}" method="post">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><strong>Role</strong> Update</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label" for="name">Name</label>
                                <div class="col-md-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                        <input type="text" class="form-control" name="name" value="{{$data->name}}" id="name" placeholder="name"/>
                                    </div>
                                    <span class="help-block">Role name</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label" for="description">Description</label>
                                <div class="col-md-6 col-xs-12">
                                    <textarea class="form-control" id="description" name="description" rows="3">{{ $data->description }}</textarea>
                                    <span class="help-block">description</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label" for="rules">Rules</label>
                                <div class="col-md-9 col-xs-12">
                                    <div class="panel-body">
                                        @foreach($modules as $mod)
                                        <legend>{{ strtoupper($mod->module) }}</legend>
                                        <?php
                                        $group = App\Models\master\rule::select(array('group'))->normal()->where('module', '=', $mod->module)
                                                        //->where('group','!=','*')
                                                        ->groupBy('group')->orderBy('group', 'asc')->get();
                                        ?>
                                        @foreach($group as $grp)
                                            @if($grp->group == '*')
                                            <?php
                                            $status = App\Models\master\rule::select(array('id', 'group'))->normal()->where('module', '=', $mod->module)
                                                            ->where('group', '=', '*')->first();
                                            ?>
                                            <div class="form-group">
                                                <label for="description" class="col-sm-2 control-label"></label>
                                                <div class="col-sm-3 check-form-{{$status->id}}">
                                                    <div class="">
                                                        <label>
                                                            <input type="checkbox" class=" checked-{{$status->id}}" name="rules[]" value="{{$status->id}}"> All Grant
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                            <?php
                                            $status = App\Models\master\rule::select(array('id', 'group', 'action'))->normal()->where('module', '=', $mod->module)
                                                            ->where('group', '=', $grp->group)->orderBy('action', 'asc')->get();
                                            ?>
                                            <div class="form-group">
                                                <label for="description" class="col-sm-2 control-label">Menu {{$grp->group}}</label>
                                                @foreach($status as $st)
                                                <div class="col-sm-2 check-form-{{$st->id}}">
                                                    <div class="">
                                                        <label>
                                                            <input class="checked-{{$st->id}}" type="checkbox" name="rules[]" value="{{$st->id}}"> @if($st->action == '*') All Grant @else{{$st->action}} @endif
                                                        </label>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                            @endif
                                            @endforeach
                                            <hr>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-default" type="reset">Clear Form</button>                                    
                            <button class="btn btn-success pull-right">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
    $(document).ready(function(){
    @foreach($action_aktif as $r)
        $(".checked-{{$r->id}}").attr("checked", "checked");
        $('.check-form-{{$r->id}} span').addClass('checked')
    @endforeach
    });
</script>
@stop