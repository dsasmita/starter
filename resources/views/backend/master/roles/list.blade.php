@extends('backend/template')
@section('title', $title)

@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <h4 class="heading_a uk-margin-bottom">ROLES</h4>
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <a href="{{ route('backMasterRolesCreate') }}" class="md-btn md-btn-primary"><i class="material-icons">tune</i> New Role</a>
                <div class="uk-overflow-container">
                    <table id="dt_scroll" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Nama Akses</th>
                                <th>Description</th>
                                <th>Modified By</th>
                                <th width="100px">Action</th>   
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($lists as $data)
                            <tr>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->description }}</td>
                                <td>
                                    @if($data->modifiedBy)
                                        {{$data->modifiedBy->username}}<br>
                                    @endif
                                    {{date('F jS \\a\\t g:ia',strtotime($data->updated_at))}}
                                </td>
                                <td>
                                    <div class="btn-group btn-group-sm">
                                        <a href="{{ route('backMasterRolesUpdate',$data->id) }}" title="Update" class="btn btn-info"><i class="material-icons md-icon md-24">&#xE254;</i></a>
                                        <a onclick="return confirm('Apakah anda yakin?')" href="{{ route('backMasterRolesDelete',$data->id) }}" title="Delete" class="btn btn-danger"><i class="material-icons md-24 uk-text-danger md-icon">&#xE872;</i></a>                                                                                     
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <?php echo $lists->render() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<script>
    //put all about javascript here
</script>
@stop