@extends('backend/template')

@section('title', $title)

@section('css')
@stop

@section('content')
<!-- BEGIN PAGE HEADER-->
<div id="page_content">
    <div id="page_content_inner">

        <!-- statistics (small charts) -->
        <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                        <span class="uk-text-muted uk-text-small">Visitors (last 7d)</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>12456</noscript></span></h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                        <span class="uk-text-muted uk-text-small">Sale</span>
                        <h2 class="uk-margin-remove">$<span class="countUpMe">0<noscript>142384</noscript></span></h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_orders peity_data">64/100</span></div>
                        <span class="uk-text-muted uk-text-small">Orders Completed</span>
                        <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>64</noscript></span>%</h2>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-float-right uk-margin-top uk-margin-small-right"><span class="peity_live peity_data">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,7,3,5,2</span></div>
                        <span class="uk-text-muted uk-text-small">Visitors (live)</span>
                        <h2 class="uk-margin-remove" id="peity_live_text">46</h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- circular charts -->
        <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-5 uk-text-center uk-sortable sortable-handler" id="dashboard_sortable_cards" data-uk-sortable data-uk-grid-margin>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content">
                        <div class="epc_chart" data-percent="76" data-bar-color="#03a9f4">
                            <span class="epc_chart_icon"><i class="material-icons">&#xE0BE;</i></span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                User Messages
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias consectetur.
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content uk-flex uk-flex-center uk-flex-middle">
                        <span class="peity_conversions_large peity_data">5,3,9,6,5,9,7</span>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                Conversions
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay md-card-overlay-active">
                    <div class="md-card-content" id="canvas_1">
                        <div class="epc_chart" data-percent="37" data-bar-color="#9c27b0">
                            <span class="epc_chart_icon"><i class="material-icons">&#xE85D;</i></span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                Tasks List
                            </h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <button class="md-btn md-btn-primary">More</button>
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content">
                        <div class="epc_chart" data-percent="53" data-bar-color="#009688">
                            <span class="epc_chart_text"><span class="countUpMe">53</span>%</span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                Orders
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </div>
                </div>
            </div>
            <div>
                <div class="md-card md-card-hover md-card-overlay">
                    <div class="md-card-content">
                        <div class="epc_chart" data-percent="37" data-bar-color="#607d8b">
                            <span class="epc_chart_icon"><i class="material-icons">&#xE7FE;</i></span>
                        </div>
                    </div>
                    <div class="md-card-overlay-content">
                        <div class="uk-clearfix md-card-overlay-header">
                            <i class="md-icon material-icons md-card-overlay-toggler">&#xE5D4;</i>
                            <h3>
                                User Registrations
                            </h3>
                        </div>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    </div>
                </div>
            </div>
        </div>

        <!-- tasks -->
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-overflow-container">
                            <table class="uk-table">
                                <thead>
                                    <tr>
                                        <th class="uk-text-nowrap">Task</th>
                                        <th class="uk-text-nowrap">Status</th>
                                        <th class="uk-text-nowrap">Progress</th>
                                        <th class="uk-text-nowrap uk-text-right">Due Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="uk-table-middle">
                                        <td class="uk-width-3-10 uk-text-nowrap"><a href="scrum_board.html">ALTR-231</a></td>
                                        <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge">In progress</span></td>
                                        <td class="uk-width-3-10">
                                            <div class="uk-progress uk-progress-mini uk-progress-warning uk-margin-remove">
                                                <div class="uk-progress-bar" style="width: 40%;"></div>
                                            </div>
                                        </td>
                                        <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">24.11.2015</td>
                                    </tr>
                                    <tr class="uk-table-middle">
                                        <td class="uk-width-3-10 uk-text-nowrap"><a href="scrum_board.html">ALTR-82</a></td>
                                        <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-warning">Open</span></td>
                                        <td class="uk-width-3-10">
                                            <div class="uk-progress uk-progress-mini uk-progress-success uk-margin-remove">
                                                <div class="uk-progress-bar" style="width: 82%;"></div>
                                            </div>
                                        </td>
                                        <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">21.11.2015</td>
                                    </tr>
                                    <tr class="uk-table-middle">
                                        <td class="uk-width-3-10 uk-text-nowrap"><a href="scrum_board.html">ALTR-123</a></td>
                                        <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-primary">New</span></td>
                                        <td class="uk-width-3-10">
                                            <div class="uk-progress uk-progress-mini uk-margin-remove">
                                                <div class="uk-progress-bar" style="width: 0;"></div>
                                            </div>
                                        </td>
                                        <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">12.11.2015</td>
                                    </tr>
                                    <tr class="uk-table-middle">
                                        <td class="uk-width-3-10 uk-text-nowrap"><a href="scrum_board.html">ALTR-164</a></td>
                                        <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-success">Resolved</span></td>
                                        <td class="uk-width-3-10">
                                            <div class="uk-progress uk-progress-mini uk-progress-primary uk-margin-remove">
                                                <div class="uk-progress-bar" style="width: 61%;"></div>
                                            </div>
                                        </td>
                                        <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">17.11.2015</td>
                                    </tr>
                                    <tr class="uk-table-middle">
                                        <td class="uk-width-3-10 uk-text-nowrap"><a href="scrum_board.html">ALTR-123</a></td>
                                        <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-danger">Overdue</span></td>
                                        <td class="uk-width-3-10">
                                            <div class="uk-progress uk-progress-mini uk-progress-danger uk-margin-remove">
                                                <div class="uk-progress-bar" style="width: 10%;"></div>
                                            </div>
                                        </td>
                                        <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">12.11.2015</td>
                                    </tr>
                                    <tr class="uk-table-middle">
                                        <td class="uk-width-3-10"><a href="scrum_board.html">ALTR-92</a></td>
                                        <td class="uk-width-2-10"><span class="uk-badge uk-badge-success">Open</span></td>
                                        <td class="uk-width-3-10">
                                            <div class="uk-progress uk-progress-mini uk-margin-remove">
                                                <div class="uk-progress-bar" style="width: 90%;"></div>
                                            </div>
                                        </td>
                                        <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small">08.11.2015</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
                        <h3 class="heading_a uk-margin-bottom">Statistics</h3>
                        <div id="ct-chart" class="chartist"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

@section('javascript')
<!-- d3 -->
        <script src="{{asset('assets')}}/bower_components/d3/d3.min.js"></script>
        <!-- metrics graphics (charts) -->
        <script src="{{asset('assets')}}/bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
        <!-- chartist (charts) -->
        <script src="{{asset('assets')}}/bower_components/chartist/dist/chartist.min.js"></script>
        <!-- maplace (google maps) -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="{{asset('assets')}}/bower_components/maplace.js/src/maplace-0.1.3.js"></script>
        <!-- peity (small charts) -->
        <script src="{{asset('assets')}}/bower_components/peity/jquery.peity.min.js"></script>
        <!-- easy-pie-chart (circular statistics) -->
        <script src="{{asset('assets')}}/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <!-- countUp -->
        <script src="{{asset('assets')}}/bower_components/countUp.js/countUp.min.js"></script>
        <!-- handlebars.js -->
        <script src="{{asset('assets')}}/bower_components/handlebars/handlebars.min.js"></script>
        <script src="{{asset('assets')}}/assets/js/custom/handlebars_helpers.min.js"></script>
        <!-- CLNDR -->
        <script src="{{asset('assets')}}/bower_components/clndr/src/clndr.js"></script>
        <!-- fitvids -->
        <script src="{{asset('assets')}}/bower_components/fitvids/jquery.fitvids.js"></script>

        <!--  dashbord functions -->
        <script src="{{asset('assets')}}/assets/js/pages/dashboard.min.js"></script>
@stop