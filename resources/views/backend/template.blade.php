<!doctype html>
<!--[if lte IE 9]> 
<html class="lte-ie9" lang="en">
   <![endif]-->
   <!--[if gt IE 9]><!--> 
   <html lang="en">
      <!--<![endif]-->
      <head>
         <meta charset="UTF-8">
         <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
         <meta http-equiv="X-UA-Compatible" content="IE=edge">
         <!-- Remove Tap Highlight on Windows Phone IE -->
         <meta name="msapplication-tap-highlight" content="no"/>
         <link rel="shortcut icon" href="{{asset('assets')}}/assets/img/add/favicon.png">
         <title>{{$title}}</title>
         <!-- additional styles for plugins -->
         <!-- weather icons -->
         <link rel="stylesheet" href="{{asset('assets')}}/bower_components/weather-icons/css/weather-icons.min.css" media="all">
         <!-- metrics graphics (charts) -->
         <link rel="stylesheet" href="{{asset('assets')}}/bower_components/metrics-graphics/dist/metricsgraphics.css">
         <!-- chartist -->
         <link rel="stylesheet" href="{{asset('assets')}}/bower_components/chartist/dist/chartist.min.css">
         <!-- uikit -->
         <link rel="stylesheet" href="{{asset('assets')}}/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
         <!-- flag icons -->
         <link rel="stylesheet" href="{{asset('assets')}}/assets/icons/flags/flags.min.css" media="all">
         <!-- altair admin -->
         <link rel="stylesheet" href="{{asset('assets')}}/assets/css/main.min.css" media="all">
         <!-- matchMedia polyfill for testing media queries in JS -->
         @yield('css')
         <!--[if lte IE 9]>
         <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
         <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
         <![endif]-->
      </head>
      <body class="{{ $sidebar_status ? 'sidebar_main_open' : 'sidebar_main_close'  }} sidebar_main_swipe">
         <!-- main header -->
         <header id="header_main">
            <div class="header_main_content">
               <nav class="uk-navbar">
                  <!-- main sidebar switch -->
                  <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                  <span class="sSwitchIcon"></span>
                  </a>
                  @include('backend/sidebar/sidebar-dropdown')
                  <div class="uk-navbar-flip">
                     <ul class="uk-navbar-nav user_actions">
                        <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                           <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">16</span></a>
                           <div class="uk-dropdown uk-dropdown-xlarge">
                              <div class="md-card-content">
                                 <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                                    <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                                 </ul>
                                 <ul id="header_alerts" class="uk-switcher uk-margin">
                                    <li>
                                       <ul class="md-list md-list-addon">
                                          <li>
                                             <div class="md-list-addon-element">
                                                <span class="md-user-letters md-bg-cyan">jx</span>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading"><a href="pages_mailbox.html">Iure et nam.</a></span>
                                                <span class="uk-text-small uk-text-muted">Vel aut officiis commodi veritatis dicta.</span>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="md-list-addon-element">
                                                <img class="md-user-image md-list-addon-avatar" src="{{asset('assets')}}/assets/img/avatars/avatar_07_tn.png" alt=""/>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading"><a href="pages_mailbox.html">Voluptate explicabo.</a></span>
                                                <span class="uk-text-small uk-text-muted">Doloribus qui expedita debitis saepe quidem vel impedit dolore dolore qui est.</span>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="md-list-addon-element">
                                                <span class="md-user-letters md-bg-light-green">lm</span>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading"><a href="pages_mailbox.html">Minima perspiciatis.</a></span>
                                                <span class="uk-text-small uk-text-muted">Quas cumque doloremque ab aut aut quia veritatis.</span>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="md-list-addon-element">
                                                <img class="md-user-image md-list-addon-avatar" src="{{asset('assets')}}/assets/img/avatars/avatar_02_tn.png" alt=""/>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading"><a href="pages_mailbox.html">Aperiam dolorum corporis.</a></span>
                                                <span class="uk-text-small uk-text-muted">Delectus dicta eaque illum ad nemo.</span>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="md-list-addon-element">
                                                <img class="md-user-image md-list-addon-avatar" src="{{asset('assets')}}/assets/img/avatars/avatar_09_tn.png" alt=""/>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading"><a href="pages_mailbox.html">Non harum unde.</a></span>
                                                <span class="uk-text-small uk-text-muted">Sequi molestias non provident similique adipisci ullam.</span>
                                             </div>
                                          </li>
                                       </ul>
                                       <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                          <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                       </div>
                                    </li>
                                    <li>
                                       <ul class="md-list md-list-addon">
                                          <li>
                                             <div class="md-list-addon-element">
                                                <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading">Sit doloribus voluptas.</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Harum velit assumenda dolorem ducimus ut officiis.</span>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="md-list-addon-element">
                                                <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading">Molestias libero.</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Magnam enim quia aut tenetur error numquam.</span>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="md-list-addon-element">
                                                <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading">Et et.</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Aliquam eius id qui illum.</span>
                                             </div>
                                          </li>
                                          <li>
                                             <div class="md-list-addon-element">
                                                <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                             </div>
                                             <div class="md-list-content">
                                                <span class="md-list-heading">Non ad.</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Et inventore aperiam velit.</span>
                                             </div>
                                          </li>
                                       </ul>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </li>
                        <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                           <a href="#" class="user_action_image">
                              @if($user->avatar != '')
                              <img class="md-user-image" src="{{asset('')}}/{{$user->avatar}}" alt="avatar"/>
                              @else
                              <img class="md-user-image" src="{{asset('assets')}}/assets/img/avatars/avatar_tn.png" alt="avatar"/>
                              @endif
                            </a>
                           <div class="uk-dropdown uk-dropdown-small">
                              <ul class="uk-nav js-uk-prevent">
                                 <li><a href="{{route('backProfil')}}">Profil</a></li>
                                 <li><a href="{{route('backLogout')}}">Logout</a></li>
                              </ul>
                           </div>
                        </li>
                     </ul>
                  </div>
               </nav>
            </div>
         </header>
         <!-- main header end -->
         <aside id="sidebar_main">
            <div class="sidebar_main_header">
               <div class="sidebar_logo">
                  <a href="index.html" class="sSidebar_hide"><img src="{{asset('assets')}}/assets/img/add/logo.png" alt="" height="15" width="71"/></a>
                  <a href="index.html" class="sSidebar_show"><img src="{{asset('assets')}}/assets/img/add/logo-small.png" alt="" height="32" width="32"/></a>
               </div>
            </div>
            <div class="menu_section">
               <ul>
                  @include('backend/sidebar/sidebar-all')
               </ul>
            </div>
         </aside>
         <!-- main sidebar end -->
         <!-- main header end -->
         @yield('content')
         <!-- google web fonts -->
       <script>
           WebFontConfig = {
               google: {
                   families: [
                       'Source+Code+Pro:400,700:latin',
                       'Roboto:400,300,500,700,400italic:latin'
                   ]
               }
           };
           (function() {
               var wf = document.createElement('script');
               wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
               '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
               wf.type = 'text/javascript';
               wf.async = 'true';
               var s = document.getElementsByTagName('script')[0];
               s.parentNode.insertBefore(wf, s);
           })();
       </script>

       <!-- common functions -->
       <script src="{{asset('assets')}}/assets/js/common.min.js"></script>
       <!-- uikit functions -->
       <script src="{{asset('assets')}}/assets/js/uikit_custom.min.js"></script>
       <!-- altair common functions/helpers -->
       <script src="{{asset('assets')}}/assets/js/altair_admin_common.min.js"></script>

       <script>
           $(function() {
               // enable hires images
               altair_helpers.retina_images();
               // fastClick (touch devices)
               if(Modernizr.touch) {
                   FastClick.attach(document.body);
               }
           });
       </script>
       @yield('javascript')
      </body>
   </html>