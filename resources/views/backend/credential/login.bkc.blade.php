<!doctype html>
<!--[if lte IE 9]> 
<html class="lte-ie9" lang="en">
	<![endif]-->
	<!--[if gt IE 9]><!--> 
	<html lang="en">
		<!--<![endif]-->
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<!-- Remove Tap Highlight on Windows Phone IE -->
			<meta name="msapplication-tap-highlight" content="no"/>
			<link rel="shortcut icon" href="{{asset('assets')}}/assets/img/add/favicon.ico">
			<title>{{$title}}</title>
			<link href='{{asset('assets')}}/assets/css/fonts-roboto.css' rel='stylesheet' type='text/css'>
			<!-- uikit -->
			<link rel="stylesheet" href="{{asset('assets')}}/bower_components/uikit/css/uikit.almost-flat.min.css"/>
			<!-- altair admin login page -->
			<link rel="stylesheet" href="{{asset('assets')}}/assets/css/login_page.css" />
		</head>
		<body class="login_page bg-login">
			<div class="login_page_wrapper">
				<div class="md-card login_styled" id="login_card">
					<div class="md-card-content large-padding" id="login_form">
						<div class="login_heading">
							<img src="{{asset('assets')}}/assets/img/add/logo.png" alt="amarbank">
						</div>
						<form action="{{ route('backLoginPost') }}" method="post">
							<input type="hidden" name="_token" value="{!! csrf_token() !!}">
							@if (!$status_login)
						        <p style="color:red">Login failed</p>
						    @endif
							<div class="uk-form-row">
								<label for="login_username">Username</label>
								<input class="md-input" type="text" id="login_username" autocomplete="off" name="username" required />
							</div>
							<div class="uk-form-row">
								<label for="login_password">Password</label>
								<input class="md-input" type="password" id="login_password" autocomplete="off" name="password" required/>
							</div>
							<div class="uk-margin-medium-top">
								<input type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large" name="login" value="Sign In">
							</div>
							<div class="uk-margin-top">
								<a href="#" id="password_reset_show" class="uk-float-right">Reset Password</a>
								<!--<span class="icheck-inline">
									<input type="checkbox" name="login_page_stay_signed" id="login_page_stay_signed" data-md-icheck />
									<label for="login_page_stay_signed" class="inline-label">Stay signed in</label>
									</span>-->
							</div>
						</form>
					</div>
					<!-- Reset Password -->
					<div class="md-card-content large-padding" id="login_password_reset" style="display: none">
						<button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
						<h2 class="heading_a uk-margin-large-bottom">Reset password</h2>
						<form>
							<div class="uk-form-row">
								<label for="login_email_reset">Your email address</label>
								<input class="md-input" type="text" id="login_email_reset" name="login_email_reset" />
							</div>
							<div class="uk-margin-medium-top">
								<a href="index.html" class="md-btn md-btn-primary md-btn-block">Reset password</a>
							</div>
						</form>
					</div>
					<br>
				</div>
			</div>
			<!-- common functions -->
			<script src="{{asset('assets')}}/assets/js/common.min.js"></script>
			<!-- altair core functions -->
			<script src="{{asset('assets')}}/assets/js/altair_admin_common.min.js"></script>
			<!-- altair login page functions -->
			<script src="{{asset('assets')}}/assets/js/pages/login.min.js"></script>
		</body>
	</html>