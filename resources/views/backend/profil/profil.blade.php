@extends('backend/template')

@section('title', $title)

@section('css')
<link href="{{asset('assets')}}/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets')}}/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="{{asset('assets')}}/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
@stop

@section('content')
<div id="page_content">
    <div id="page_content_inner">
        <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
            <div class="uk-width-large-7-10">
                <div class="md-card">
                    <div class="user_heading">
                        <div class="user_heading_avatar">
                            @if($user->avatar != '')
                            <img src="{{asset('')}}/{{$user->avatar}}" class="" alt="avatar">
                            @else
                            <img src="{{asset('assets')}}/assets/img/avatars/avatar_11.png" class="" alt="user avatar">
                            @endif
                        </div>
                        <div class="user_heading_content">
                            <h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate">{{$user->name}}</span><span class="sub-heading">{{$user->hakAKses()}}</span></h2>
                        </div>
                    </div>
                    <div class="user_content">
                        <ul id="user_profile_tabs" class="uk-tab" data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}" data-uk-sticky="{ top: 48, media: 960 }">
                            <li class="{{$tab == '1' ? 'uk-active' : ''}}"><a href="#">Personal Info</a></li>
                            <li class="{{$tab == '2' ? 'uk-active' : ''}}"><a href="#">Change Avatar</a></li>
                            <li class="{{$tab == '3' ? 'uk-active' : ''}}"><a href="#">Change Password</a></li>
                        </ul>
                        <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                            <li>
                                <h3>Personal Info</h3>
                                @if(Session::get('personal-fail'))
                                <div class="uk-alert uk-alert-warning" data-uk-alert>
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{Session::get('personal-fail')}}
                                </div>
                                @endif

                                @if(Session::get('personal-success'))
                                <div class="uk-alert uk-alert-success" data-uk-alert>
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{Session::get('personal-success')}}
                                </div>
                                @endif
                                <form role="form" action="{{route('backProfilPostPersonalInfo')}}" method="post">
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1">
                                            <div class="uk-form-row">
                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2">
                                                        <label>Username</label>
                                                        <input type="text" class="md-input" disabled="" value="{{$user->username}}" name="username"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label>Email</label>
                                                        <input type="text" class="md-input" value="{{$user->email}}" name="email"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="uk-form-row">
                                                <label>Fullname</label>
                                                <input type="text" class="md-input"  name="name" value="{{$user->name}}"/>
                                            </div>
                                            <div class="uk-form-row">
                                                <button class="md-btn md-btn-primary md-btn-block" type="submit">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <h3>Personal Info</h3>
                                @if(Session::get('avatar-fail'))
                                <div class="uk-alert uk-alert-warning" data-uk-alert>
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{Session::get('avatar-fail')}}
                                </div>
                                @endif

                                @if(Session::get('avatar-success'))
                                <div class="uk-alert uk-alert-success" data-uk-alert>
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{Session::get('avatar-success')}}
                                </div>
                                @endif
                                <form action="{{route('backProfilPostPersonalavatar')}}" role="form" method="post"  enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px;">
                                                @if($user->avatar != '')
                                                <img src="{{asset('')}}/{{$user->avatar}}" class="img-responsive" alt="avatar">
                                                @else
                                                <img src="{{asset('assets')}}/assets/img/avatars/avatar_11.png" class="" alt="user avatar">
                                                @endif
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <input type="file" name="avatar">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="md-btn md-btn-primary">
                                        Save Changes </button>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <h3>Change Password</h3>
                                @if(Session::get('password-fail'))
                                <div class="uk-alert uk-alert-warning" data-uk-alert>
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{Session::get('password-fail')}}
                                </div>
                                @endif

                                @if(Session::get('password-success'))
                                <div class="uk-alert uk-alert-success" data-uk-alert>
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{Session::get('password-success')}}
                                </div>
                                @endif
                                <form action="{{ route('backProfilPostPersonalPassword') }}" method="post">
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1">
                                            <div class="uk-form-row">
                                                <label>Current Password</label>
                                                <input type="password" class="md-input"  name="old"/>
                                            </div>
                                            <div class="uk-form-row">
                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2">
                                                        <label>New Password</label>
                                                        <input type="password" class="md-input" name="password"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2">
                                                        <label>Re-type New Password</label>
                                                        <input type="password" class="md-input" name="password_confirmation"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="uk-form-row">
                                                <button class="md-btn md-btn-primary md-btn-block" type="submit">Change</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-3-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <div class="uk-margin-medium-bottom">
                            <h3 class="heading_c uk-margin-bottom">Alerts</h3>
                            <ul class="md-list md-list-addon">
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Aut labore.</span>
                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Et quam consectetur minus dolore accusamus qui.</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Libero voluptas.</span>
                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Odio est quam animi eveniet dolores culpa sint.</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-addon-element">
                                        <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Est enim.</span>
                                        <span class="uk-text-small uk-text-muted uk-text-truncate">Aspernatur repellat cupiditate harum necessitatibus consectetur.</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
    //put all about javascript here
</script>
@stop