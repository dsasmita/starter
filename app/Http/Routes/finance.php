<?php
/**
 * Routes
 *
 * @author		Dadang Sasmita <dangs.work@gmail.com>
 *
 * @version 	1.0
 */
use App\Http\Helpers\site;

/*================= Backend ===================*/
Route::group(['middleware' => ['backend']], function() {
    
    $helperSite = new Site;
    $prefix = $helperSite->getSetting('admin_path');

    $module = 'finance';
    Route::get($prefix .'/'.$module, [
        'as' => 'backDashboardFinance',
        'uses' => 'Finance\dashboardCont@index'
    ]);

});


/*================= END ===================*/