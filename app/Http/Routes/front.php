<?php
/**
 * Routes
 *
 * @author		Dadang Sasmita <dangs.work@gmail.com>
 *
 * @version 	1.0
 */

use Illuminate\Support\Facades\Auth;

/*================= Front ===================*/
Route::get('/', function () {
	if(Auth::check()) {
		return redirect(route('backDashboard'));
	}else{
 	   return redirect(route('backLogin'));
	}
});