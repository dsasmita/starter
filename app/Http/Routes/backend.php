<?php
/**
 * Routes
 *
 * @author		Dadang Sasmita <dangs.work@gmail.com>
 *
 * @version 	1.0
 */
use App\Http\Helpers\site;


/*================= Credential ===================*/
Route::group(['middleware' => ['guest']], function() {
    $helperSite = new Site;
	$prefix = $helperSite->getSetting('admin_path');
    
    Route::get($prefix . '/login', [
        'as' => 'backLogin',
        'uses' => 'Backend\credentialCont@index'
    ]);

    Route::post($prefix . '/login', [
        'as' => 'backLoginPost',
        'uses' => 'Backend\credentialCont@loginPost'
    ]);
});

$helperSite = new Site;
$prefix = $helperSite->getSetting('admin_path');

Route::get($prefix . '/logout', [
    'as' => 'backLogout',
    'uses' => 'Backend\credentialCont@logout'
]);

/*================= Backend ===================*/
Route::group(['middleware' => ['backend']], function() {
    
    $helperSite = new Site;
    $prefix = $helperSite->getSetting('admin_path');

    Route::get($prefix , [
        'as' => 'backDashboard',
        'uses' => 'Backend\homeCont@index'
    ]);

    Route::get($prefix . '/dashboard', [
        'as' => 'backDashboard',
        'uses' => 'Backend\homeCont@index'
    ]);

    //++++++++++++++++++++++++++++++++++++++++++ User Action
    $module = 'profil';
    Route::get($prefix .'/'.$module, [
        'as' => 'backProfil',
        'uses' => 'Backend\profilCont@index'
    ]);

    Route::get($prefix .'/'.$module.'/personal-info', [
        'as' => 'backProfilInfo',
        'uses' => 'Backend\profilCont@index'
    ]);

    Route::post($prefix .'/'.$module.'/personal-info', [
        'as' => 'backProfilPostPersonalInfo',
        'uses' => 'Backend\profilCont@personalInfo'
    ]);

    Route::get($prefix .'/'.$module.'/avatar', [
        'as' => 'backProfilAvatar',
        'uses' => 'Backend\profilCont@index'
    ]);

    Route::post($prefix .'/'.$module.'/avatar', [
        'as' => 'backProfilPostPersonalavatar',
        'uses' => 'Backend\profilCont@personalAvatar'
    ]);

    Route::post($prefix .'/'.$module.'/change-password', [
        'as' => 'backProfilPostPersonalPassword',
        'uses' => 'Backend\profilCont@personalPassword'
    ]);

    //+++++++++++++++++++++++++++++++++++++++++++master data
    $module = 'master';
    Route::get($prefix .'/'.$module, [
        'as' => 'backDashboardMaster',
        'uses' => 'Master\settingCont@dashboard'
    ]);

    //======================================== master settings
    Route::get($prefix .'/'.$module.'/setting', [
        'as' => 'backMasterSettings',
        'uses' => 'Master\settingCont@index'
    ]);

    Route::post($prefix .'/'.$module.'/setting/general', [
        'as' => 'backMasterSettingsGeneralPost',
        'uses' => 'Master\settingCont@generalAction'
    ]);


    //======================================== user
    Route::get($prefix .'/'.$module.'/users', [
        'as' => 'backMasterUsers',
        'uses' => 'Master\userCont@index'
    ]);

    Route::get($prefix .'/'.$module.'/users/create', [
        'as' => 'backMasterUsersCreate',
        'uses' => 'Master\userCont@create'
    ]);

    Route::post($prefix .'/'.$module.'/users/create', [
        'as' => 'backMasterUsersCreatePost',
        'uses' => 'Master\userCont@createPost'
    ]);

    Route::get($prefix .'/'.$module.'/users/update/{id}', [
        'as' => 'backMasterUsersUpdate',
        'uses' => 'Master\userCont@update'
    ])->where(['id' => '[0-9]+']);

    Route::post($prefix .'/'.$module.'/users/update/{id}', [
        'as' => 'backMasterUsersUpdatePost',
        'uses' => 'Master\userCont@updatePost'
    ])->where(['id' => '[0-9]+']);

    Route::get($prefix .'/'.$module.'/users/delete/{id}', [
        'as' => 'backMasterUsersDelete',
        'uses' => 'Master\userCont@delete'
    ])->where(['id' => '[0-9]+']);


    //=============================================== roles
    Route::get($prefix .'/'.$module.'/roles', [
        'as' => 'backMasterRoles',
        'uses' => 'Master\rolesCont@index'
    ]);

    Route::get($prefix .'/'.$module.'/roles/create', [
        'as' => 'backMasterRolesCreate',
        'uses' => 'Master\rolesCont@create'
    ]);

    Route::post($prefix .'/'.$module.'/roles/create', [
        'as' => 'backMasterRolesCreatePost',
        'uses' => 'Master\rolesCont@createPost'
    ]);

    Route::get($prefix .'/'.$module.'/roles/update/{id}', [
        'as' => 'backMasterRolesUpdate',
        'uses' => 'Master\rolesCont@update'
    ])->where(['id' => '[0-9]+']);

    Route::post($prefix .'/'.$module.'/roles/update/{id}', [
        'as' => 'backMasterRolesUpdatePost',
        'uses' => 'Master\rolesCont@updatePost'
    ])->where(['id' => '[0-9]+']);

    Route::get($prefix .'/'.$module.'/roles/delete/{id}', [
        'as' => 'backMasterRolesDelete',
        'uses' => 'Master\rolesCont@delete'
    ])->where(['id' => '[0-9]+']);

    //=================================================== rules
    Route::get($prefix .'/'.$module.'/rules', [
        'as' => 'backMasterRules',
        'uses' => 'Master\rulesCont@index'
    ]);

    Route::get($prefix .'/'.$module.'/rules/create', [
        'as' => 'backMasterRulesCreate',
        'uses' => 'Master\rulesCont@create'
    ]);

    Route::post($prefix .'/'.$module.'/rules/create', [
        'as' => 'backMasterRulesCreatePost',
        'uses' => 'Master\rulesCont@createPost'
    ]);

    Route::get($prefix .'/'.$module.'/rules/update/{id}', [
        'as' => 'backMasterRulesUpdate',
        'uses' => 'Master\rulesCont@update'
    ])->where(['id' => '[0-9]+']);

    Route::post($prefix .'/'.$module.'/rules/update/{id}', [
        'as' => 'backMasterRulesUpdatePost',
        'uses' => 'Master\rulesCont@updatePost'
    ])->where(['id' => '[0-9]+']);

    Route::get($prefix .'/'.$module.'/rules/delete/{id}', [
        'as' => 'backMasterRulesDelete',
        'uses' => 'Master\rulesCont@delete'
    ])->where(['id' => '[0-9]+']);
});


/*================= END ===================*/