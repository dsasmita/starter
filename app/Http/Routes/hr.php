<?php
/**
 * Routes
 *
 * @author		Dadang Sasmita <dangs.work@gmail.com>
 *
 * @version 	1.0
 */
use App\Http\Helpers\site;

/*================= Backend ===================*/
Route::group(['middleware' => ['backend']], function() {
    
    $helperSite = new Site;
    $prefix = $helperSite->getSetting('admin_path');

    $module = 'hr';
    Route::get($prefix .'/'.$module, [
        'as' => 'backDashboardHr',
        'uses' => 'Hr\dashboardCont@index'
    ]);

});


/*================= END ===================*/