<?php namespace App\Http\Helpers;

use App\Http\Libraries\superconfig\superconfig;

/**
 * Class->Helper site.
 * 
 * @author Dadang S <dangs.work@gmail.com>
 * @version 1.0
 *
 */

Class Site{
	private $superConfig;

	public function __construct() {
		$this->superConfig = new SuperConfig;
	}

	public function getSiteName(){
		return $this->superConfig->get_config('site_name');
	}

	public function setPageTitle($pagetitle) {
		return $pagetitle . ' | ' . $this->getSiteName();
	}

	function getSetting($key=null) {
		return $this->superConfig->get_config($key);
	}

	public function updateSetting($key = null,$value = null){
		return $this->superConfig->update_setting($key, $value);
	}

	public function dateRange( $first, $last, $step = '+1 day', $format = 'Y/m/d' ) {

		$dates = array();
		$current = strtotime( $first );
		$last = strtotime( $last );

		while( $current <= $last ) {

			$dates[] = date( $format, $current );
			$current = strtotime( $step, $current );
		}

		return $dates;
	}

	public function bank_list(){
		$banks = array();
        if (($handle = fopen("assets/file/bank_helper.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 150, ";")) !== FALSE) {
                $banks[] = array("code" => $data[1], "name" => $data[2]);
            }
            fclose($handle);
        }
        foreach ($banks as $key => $row) {
            $code[$key]  = $row['code'];
            $name[$key] = $row['name'];
        }
        array_multisort($name, SORT_ASC, $code, SORT_ASC, $banks);

        return $banks;
	}

	public function validateDate($date){
        $d = \DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }
}