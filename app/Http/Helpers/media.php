<?php

/**
 * Get parsed link for view with special profile
 *
 * @param string $link
 * @param string $profile
 */
function getParsedLink($link, $profile = null, $is_S3 = false) {
	$link = str_replace('/', DS, $link);
	$path = $link;
	$name = pathinfo($link, PATHINFO_FILENAME);
	$ext = pathinfo($link, PATHINFO_EXTENSION);
	$link = pathinfo($link, PATHINFO_DIRNAME);
	//$template_profile = Alpha\Site::getThumbnail();
	$link = str_replace(DS, '/', $link);

	if($is_S3) {
		if(empty($profile) || empty($template_profile[$profile])) {
			$link = $link.'/'.$name.'.'.$ext;
			return '/'.$link;
		} else {
			if(array_key_exists($profile, Alpha\Site::getThumbnail())) {
				$link = $link.'/'.$name.'_'.$profile.'.'.$ext;
				if(strpos($link, substr(Config::get('application.asset_url'), 1).'/uploads') !== false) {
					$file_path = substr($link,strpos($link, substr(Config::get('application.asset_url'), 1).'/uploads')+strlen(substr(Config::get('application.asset_url'), 1).'/uploads'));
					$file_path = str_replace('/', DS, $file_path);
					if(file_exists(path('public').'uploads'.$file_path))
					{
						$file_path = str_replace(DS, '/', $file_path);
						return '/uploads'.$file_path;
					} else
					{
						$src = explode(DS, $file_path);
						$src[count($src)-1] = $name.'.'.$ext;
						$src = implode(DS, $src);
						$src = 'uploads'.$src;
						imager_resizer($src,'uploads'.$file_path, $template_profile[$profile]['width'], $template_profile[$profile]['height'],$template_profile[$profile]['crop']);
						$file_path = str_replace(DS, '/', $file_path);
						return '/uploads'.$file_path;
					}
				}
			} else
				return false;
		}
	} else {
		if(empty($profile)) {
			$link = $link.'/'.$name.'.'.$ext;
		} else {
			$link = $link.'/'.$name.'_'.$profile.'.'.$ext;
			/*
			if(array_key_exists($profile, Alpha\Site::getThumbnail()))
				$link = $link.'/'.$name.'_'.$profile.'.'.$ext;
			else
				return false;
			*/
		}
		return $link;
	}
}