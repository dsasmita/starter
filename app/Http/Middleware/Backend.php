<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Backend
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->auth->check()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }else{
            $module = $request->segment(2);
            $group = $request->segment(3);
            
            $us = $this->auth->user();
            $status = true;
            
            if($module == 'dashboard' || $module == 'profil'){
                $status = true;
            }else{
                //check module status
                if($group == null)
                {
                    $status = $us->checkAksesModule($module);
                }else
                //check module status
                {
                    $status = $us->chechAksesGroupModule($module,$group);
                }
            }
        }

        //check status
        if (!$status) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect(route('backDashboard'));
            }
        }

        return $next($request);
        /*
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }

        return $next($request);
        */
    }
}
