<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Input;
use Validator;

use App\Models\operation\agent;
//helpers
use App\Http\Helpers\site;

class agentCont extends Controller
{

    public function __construct()
    {
      //pake middleware bawaannya laravel, asumsiku semua method harus pake middleware ini
      $this->middleware('auth.basic');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['status'=>'index'];
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = ['status'=>'create'];
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $helperSite = new Site;
        $input = Input::all();
        
        $validator = Validator::make(
                        $input, [
                                'referral_code' => 'required|alpha_num|unique:'.$helperSite->getSetting('setting_db').'.operation.agents,referral_code',
                                'name' => 'required',
                                'type' => 'required',
                                'id_number' => 'required|numeric',
                                'bank_name' => 'required',
                                'bank_account' => 'required|numeric',
                                'bank_accname' => 'required',
                                'address' => 'required',
                                'email' => 'required|email',
                                'picemail' => 'email',
                                'npwp' => 'numeric'
                            ]
        );

        if ($validator->fails()) {
            return response()->json(['status'=>'not-ok','message' => 'not-ok' ]);
        }

        $data = new agent();
        $data->status = 'need-set-up';
        //generate referral code 
        //$tempslug = str_slug($input['referralcode'], ".");
        $tempslug = strtoupper($input['referral_code']);
        $oldtitle = $tempslug;
        $newtitle = $tempslug;
        $curtitle = agent::where('referral_code',$newtitle)->first();
        $counter = 1;
        while ($curtitle) {
            $newtitle = $oldtitle . $counter;
            $counter++;
            $curtitle = agent::where('referral_code',$newtitle)->first();
        }
        //end slug action

        $data->referral_code = $newtitle;
        $data->join_date = date('Y-m-d');
        $data->name = $input['name'];
        $data->type = $input['type'];
        $data->id_number = $input['id_number'];
        $data->npwp = $input['npwp'];
        $data->bank_name = $input['bank_name'];
        $data->bank_account = $input['bank_account'];
        $data->bank_accname = $input['bank_accname'];
        $data->address = $input['address'];
        $data->phone = $input['phone'];
        $data->email = $input['email'];
        $data->pic_name = $input['pic_name'];
        $data->pic_phone = $input['pic_phone'];
        $data->pic_email = $input['pic_email'];

        $data->created_by = 'userapi';
        $data->updated_by = 'userapi';

        $data->save();

        return response()->json(['status'=>'OK','message' => 'everything just OK']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        $par = Input::all();
        $agent = [];
        if($id == md5('referral_code')){
            if(isset($par['keyword'])){
                $agent = agent::where('referral_code',$par['keyword'])->first();
            }
        }else if($id == md5('id_number')){
            if(isset($par['keyword'])){
                $agent = agent::where('id_number',$par['keyword'])->first();
            }
        }else{
            if(is_numeric($id))
                $agent = agent::find($id);
        }
        if(!$agent){
            return Response::json([
                'status' => 'OK',
                'found' => '0'
              ],200);
        }

        return Response::json([
            'status' => 'OK',
            'found' => '1',
            'data' => $agent
          ],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
