<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Backend\backendBase;
use App\Models\master\user;
use App\Models\master\role;
use Illuminate\Support\Facades\Auth;
use Input;
Use Validator;
use Session;
use Illuminate\Support\Facades\Hash;

class userCont extends backendBase {

    public function index() {
        $status = Input::get('status', 'show');
        if ($status == 'show')
            $lists = user::normal()->orderBy('name')->paginate($this->limit);
        else if ($status == 'deleted')
            $lists = user::normal()->orderBy('name')->onlyTrashed()->paginate($this->limit);

        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'user',
            'action' => 'view'
        );

        $this->arr_data['status'] = $status;
        $this->arr_data['lists'] = $lists;
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Master User');
        return view('backend/master/user/list', $this->arr_data);
    }

    public function create() {
        $roles = role::select(array('id', 'name'))->normal()->orderBy('name', 'asc')->get();

        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'user',
            'action' => 'create'
        );

        $arr_data = array(
            'title' => 'Master User Create',
            'user' => $this->user,
            'roles' => $roles,
            'position' => ''
        );
        $this->arr_data['roles'] = $roles;
        return view('backend/master/user/create', $this->arr_data);
    }

    public function createPost() {
        $username = Input::get('username');
        $name = Input::get('name');
        $nik = Input::get('nik');
        $status = Input::get('status');
        $role = Input::get('role',0);
        $access_ip = Input::get('access_ip');

        $validator = Validator::make(
                        [
                    'username' => $username,
                    'name' => $name,
                    'nik' => $nik,
                    //'role' => $role,
                    'access_ip' => $access_ip
                        ], [
                    'username' => 'required|unique:users,username',
                    'name' => 'required',
                    'nik' => 'required',
                    //'role' => 'required',
                    'access_ip' => 'required'
                        ]
        );

        if ($validator->fails()) {
            return redirect(route('backMasterUsersCreate'))
                            ->withErrors($validator)->withInput();
            ;
        }

        $data = new user();
        $data->username = $username;
        $data->nik = $nik;
        $data->name = $name;
        $data->status = $status;
        $data->access_ip = $access_ip;
        $data->password = Hash::make($username . '1234');
        $data->save();

        if($role != 0)
            $data->roles()->sync(array($role));

        Session::flash('status-create', 'Create user success.');
        return redirect(route('backMasterUsers'));
    }

    public function update($id = null) {
        if (!$this->user->checkActionGroup('master', 'user', 'update')) {
            return redirect(route('master'));
        }
        $data = user::normal()->find($id);

        if (!$data)
            return redirect(route('masterUserList'));

        $roles = role::select(array('id', 'name'))->normal()->orderBy('name', 'asc')->get();

        $role_aktif = '';
        if ($data->roles()->first())
            $role_aktif = $data->roles()->first()->id;

        $position = array(
            'module' => 'master',
            'menu' => 'user',
            'action' => 'update'
        );

        $arr_data = array(
            'title' => 'Master User Update',
            'user' => $this->user,
            'data' => $data,
            'roles' => $roles,
            'role_aktif' => $role_aktif,
            'position' => $position
        );

        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'user',
            'action' => 'update'
        );
        $this->arr_data['roles'] = $roles;
        $this->arr_data['role_aktif'] = $role_aktif;
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Update User '.$data->name);
        $this->arr_data['data'] = $data;
        return view('backend/master/user/update', $this->arr_data);
    }

    public function updatePost($id = null) {
        if (!$this->user->checkActionGroup('master', 'user', 'update')) {
            return redirect(route('master'));
        }
        $data = user::normal()->find($id);

        if (!$data)
            return redirect(route('masterUserList'));

        $name = Input::get('name');
        $nik = Input::get('nik');
        $status = Input::get('status');
        $role = Input::get('role',0);
        $reset = Input::get('reset', 'no');
        $access_ip = Input::get('access_ip');

        $validator = Validator::make(
                        [
                    'name' => $name,
                    'nik' => $nik,
                    'role' => $role,
                    'access_ip' => $access_ip
                        ], [
                    'name' => 'required',
                    'nik' => 'required',
                    //'role' => 'required',
                    'access_ip' => 'required'
                        ]
        );

        if ($validator->fails()) {
            return redirect(route('backMasterUsersUpdate', $data->id))
                            ->withErrors($validator);
        }

        $data->nik = $nik;
        $data->name = $name;
        $data->status = $status;
        $data->access_ip = $access_ip;

        if ($reset == 'yes')
            $data->password = Hash::make($data->username);
        $data->save();

        if($role != 0)
            $data->roles()->sync(array($role));

        Session::flash('status-update', 'Update user success.');
        return redirect(route('backMasterUsersUpdate', $data->id));
    }

    public function delete($id = null) {
        $action = Input::get('action', 'delete');

        if ($action == 'delete') {
            $data = user::normal()->find($id);
            if (!$data)
                return redirect(route('backMasterUsers'));

            $data->delete();

            Session::flash('status-create', 'Delete user success.');
        }else if ($action == 'restore') {
            $data = user::normal()->onlyTrashed()->find($id);
            if (!$data)
                return redirect(route('backMasterUsers'));

            $data->restore();

            Session::flash('status-create', 'Restore user success.');
        }
        return redirect(route('backMasterUsers'));
    }

}
