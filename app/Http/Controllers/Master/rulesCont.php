<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Backend\backendBase;
use App\Models\master\rule;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;

class rulesCont extends backendBase {

    public function index() {
        $lists = rule::normal()
                        ->orderBy('module')
                        ->orderBy('group')->orderBy('action')
                        ->get();
        
        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'rules',
            'action' => 'view'
        );

        $arr_data = array(
            'title' => 'Master Rule Lists',
            'user' => $this->user,
            'lists' => $lists,
            'position' => ''
        );
        $this->arr_data['lists'] = $lists;
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Rules');
        return view('backend/master/rules/list', $this->arr_data);
    }

    public function create() {
        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'rules',
            'action' => 'create'
        );
        
        $arr_data = array(
            'title' => 'Master Rule Create',
            'user' => $this->user,
            'position' => ''
        );
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Rules Create');
        return view('backend/master/rules/create', $this->arr_data);
    }

    public function createPost() {
        $module = Input::get('module');
        $group = Input::get('group');
        $action = Input::get('action');
        $description = Input::get('description');

        $validator = Validator::make(
                        [
                    'module' => $module,
                    'group' => $group,
                    'action' => $action
                        ], [
                    'module' => 'required',
                    'group' => 'required',
                    'action' => 'required'
                        ]
        );

        if ($validator->fails()) {
            return redirect(route('backMasterRules'))
                            ->withErrors($validator)->withInput();
        }

        $data = new rule();
        $data->module = $module;
        $data->group = $group;
        $data->action = $action;
        $data->description = $description;
        $data->created_by = $this->user->username;
        $data->updated_by = $this->user->username;
        $data->save();

        Session::flash('status-create', 'Create Data Berhasil.');
        return redirect(route('backMasterRules'));
    }

    public function update($id = null) {
        $data = rule::normal()->find($id);

        if (!$data)
            return redirect(route('backMasterRules'));
        
        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'rules',
            'action' => 'update'
        );
        $this->arr_data['data'] = $data;
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Rules Update');
        return view('backend/master/rules/update', $this->arr_data);
    }

    public function updatePost($id = null) {
        $data = rule::normal()->find($id);

        if (!$data)
            return redirect(route('backMasterRules'));

        $module = Input::get('module');
        $group = Input::get('group');
        $action = Input::get('action');
        $description = Input::get('description');

        $validator = Validator::make(
                        [
                    'module' => $module,
                    'group' => $group,
                    'action' => $action
                        ], [
                    'module' => 'required',
                    'group' => 'required',
                    'action' => 'required'
                        ]
        );

        if ($validator->fails()) {
            return redirect(route('backMasterRulesUpdate', $data->id))->withErrors($validator);
        }

        $data->module = $module;
        $data->group = $group;
        $data->action = $action;
        $data->description = $description;
        $data->updated_by = $this->user->username;
        $data->save();

        Session::flash('status-update', 'Update Berhasil.');

        return redirect(route('backMasterRulesUpdate', $data->id));
    }

    public function delete($id = null) {
        $data = rule::find($id);
        $data->delete();

        Session::flash('status-create', 'Hapus data Berhasil.');
        return redirect(route('backMasterRules'));
    }

}
