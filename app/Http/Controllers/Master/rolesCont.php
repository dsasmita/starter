<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Backend\backendBase;
use App\Models\master\role;
use App\Models\master\rule;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;
use Illuminate\Support\Facades\Input;

class rolesCont extends backendBase {

    public function index() {

        $lists = role::normal()->paginate(10);

        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'roles',
            'action' => 'view'
        );

        $arr_data = array(
            'title' => 'Master Role Lists',
            'user' => $this->user,
            'lists' => $lists,
            'position' => ''
        );
        $this->arr_data['lists'] = $lists;
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Roles');
        return view('backend/master/roles/list', $this->arr_data);
    }

    public function create() {
        $modules = rule::select(array('id', 'module'))->normal()->where('group', '=', '*')->orderBy('module','asc')->get();

        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'roles',
            'action' => 'create'
        );

        $arr_data = array(
            'title' => 'Master Role Create',
            'user' => $this->user,
            'module' => $modules,
            'position' => ''
        );

        $this->arr_data['modules'] = $modules;
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Master User');
        return view('backend/master/roles/create', $this->arr_data);
    }

    public function createPost() {
        $name = Input::get('name');
        $description = Input::get('description');

        $validator = Validator::make(
                        ['name' => $name], ['name' => 'required']
        );

        if ($validator->fails()) {
            return redirect(route('masterRolesCreate'))->withErrors($validator);
        }

        $data = new role();
        $data->name = $name;
        $data->description = $description;
        $data->created_by = $this->user->username;
        $data->updated_by = $this->user->username;
        $data->save();

        $active = Input::get('rules', false);

        if ($active)
            $data->rules()->sync($active);

        Session::flash('status-create', 'Create data berhasil.');
        return redirect(route('backMasterRoles'));
    }

    public function update($id = null) {
        $data = role::normal()->find($id);
        if (!$data)
            return redirect(route('masterRoles'));

        $modules = rule::select(array('id', 'module'))->normal()->where('group', '=', '*')->get();

        $action_aktif = $data->rules()->get();

        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'roles',
            'action' => 'update'
        );

        $arr_data = array(
            'title' => 'Master Role Update',
            'user' => $this->user,
            'data' => $data,
            'module' => $modules,
            'action_aktif' => $action_aktif,
            'position' => ''
        );

        $this->arr_data['action_aktif'] = $action_aktif;
        $this->arr_data['data'] = $data;
        $this->arr_data['modules'] = $modules;
        $this->arr_data['title'] = $this->helperSite->setPageTitle('Roles Update');
        return view('backend/master/roles/update', $this->arr_data);
    }

    public function updatePost($id = null) {
        $data = role::normal()->find($id);

        if (!$data)
            return redirect(route('masterRoles'));

        $name = Input::get('name');
        $description = Input::get('description');

        $validator = Validator::make(
                        ['name' => $name], ['name' => 'required']
        );

        if ($validator->fails()) {
            return redirect(route('backMasterRolesUpdate', $data->id))
                            ->withErrors($validator)
                            ->withInput();
            ;
        }

        $data->name = $name;
        $data->description = $description;
        $data->updated_by = $this->user->username;
        $data->save();

        $active = Input::get('rules', false);

        if ($active)
            $data->rules()->sync($active);

        Session::flash('status-update', 'Update data berhasil.');
        return redirect(route('backMasterRolesUpdate', $data->id));
    }

    public function delete($id = null) {
        $data = role::find($id);
        $data->delete();

        Session::flash('status-create', 'Hapus data Berhasil.');
        return redirect(route('backMasterRoles'));
    }

}
