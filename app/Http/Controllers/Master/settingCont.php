<?php

namespace App\Http\Controllers\master;

use App\Http\Controllers\Backend\backendBase;
use App\Models\master\setting;
use Illuminate\Support\Facades\Auth;
use Validator;
use Session;
use Illuminate\Support\Facades\Input;

class settingCont extends backendBase {

    public function index() {
        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'setting',
            'action' => 'view'
        );

        $this->arr_data['title'] = $this->helperSite->setPageTitle('General Setting');
        return view('backend/master/setting/view', $this->arr_data);
    }

    public function dashboard() {

        $this->arr_data['position'] = array(
            'module' => 'master',
            'menu' => 'dashboard',
            'action' => 'view'
        );

        $this->arr_data['title'] = $this->helperSite->setPageTitle('Dashboard Master');
        return view('backend/master/dashboard', $this->arr_data);
    }

    public function generalAction(){
    	$input = Input::get();
    	$input = array_except($input,['_token']);
    	

    	foreach ($input as $key => $value) {
    		$status = $this->helperSite->updateSetting($key,$value);
    	}

    	Session::flash('update-success', 'Update setting success.');
        return redirect(route('backMasterSettings'));
    }

}
