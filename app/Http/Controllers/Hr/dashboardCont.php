<?php

namespace App\Http\Controllers\Hr;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\backendBase;

class dashboardCont extends backendBase
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $this->arr_data['position'] = array(
            'module' => 'hr',
            'menu' => 'dashboard',
            'action' => 'view'
        );

        $this->arr_data['title'] = $this->helperSite->setPageTitle('Dashboard Human Resource');
        return view('backend/hr/dashboard', $this->arr_data);
    }
}
