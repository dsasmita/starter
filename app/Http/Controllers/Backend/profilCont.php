<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\backendBase;
use Session;

use App\Models\master\user;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use Hash;

class profilCont extends backendBase {
	public function index()
	{
		$this->arr_data['tab'] = Input::get('tab',1);

		$this->arr_data['position'] = array(
            'module' => 'referral',
            'menu' => 'profil',
            'action' => 'view'
        );
        $this->arr_data['bgColor'] = 'page-container-bg-solid';
		$this->arr_data['title'] = $this->helperSite->setPageTitle('Profil');
		return view('backend/profil/profil', $this->arr_data);
	}

	public  function personalInfo(){
		$input = Input::get();

		$validator = Validator::make(
                        $input, [
                        			'email' => 'required|email',
                        			'name' => 'required'
                            ]
        );

        if ($validator->fails()) {
        	Session::flash('personal-fail', 'Update personal info failed');
            return redirect(route('backProfil').'?tab=1')
                            ->withErrors($validator)->withInput();
        }
		$usr = user::find(Auth::user()->id);
		$usr->name = $input['name'];
		$usr->email = $input['email'];
		$usr->update();

		Session::flash('personal-success', 'Update personal info success');
		return redirect(route('backProfil').'?tab=1');
	}

	public function personalAvatar(){
		$input['avatar'] = Input::file('avatar');


		$rules = [
                    'avatar' => 'required|mimes:jpeg,jpg,png,gif'
                ];

		$validator = Validator::make($input
                        , $rules
        );

        if ($validator->fails()) {
        	Session::flash('avatar-fail', 'Update avatar failed');
            return redirect(route('backProfil').'?tab=2')
                            ->withErrors($validator)->withInput();
        }

        $avatar = Input::file('avatar');
        $destinationPath = 'uploads' . DS . 'avatar' . DS . date("Y") . DS . date("m") . DS;
        $extension = $avatar->getClientOriginalExtension();
        $fileName = 'avatar-' . $this->user->id . '.' . $extension;

        $avatar->move($destinationPath, $fileName);

        Image::make($destinationPath . $fileName)->widen(400)->save();

        Image::make($destinationPath . $fileName)->resizeCanvas(400,400)->save();

        $usr = user::find(Auth::user()->id);
		$usr->avatar = $destinationPath.$fileName;
		$usr->update();

		Session::flash('avatar-success', 'Update avatar success');
		return redirect(route('backProfil').'?tab=2');
	}

	public function personalPassword(){
		$input = Input::get();

		$validator = Validator::make(
                        $input, [
                        			'old' => 'required',
                        			'password' => 'required|confirmed',
                            ]
        );

        if ($validator->fails()) {
        	Session::flash('password-fail', 'change password failed');
            return redirect(route('backProfil').'?tab=3')
                            ->withErrors($validator)->withInput();
        }

        if(!Hash::check($input['old'], $this->user->password)){
        	return 'fail';
        	Session::flash('password-fail', 'change password failed');
        	return redirect(route('backProfil').'?tab=3');
        }
       	
       	$usr = user::find(Auth::user()->id);
		$usr->password =Hash::make($input['password']);
		$usr->update();

        Session::flash('password-success', 'change password success');
		return redirect(route('backProfil').'?tab=3');
	}
}