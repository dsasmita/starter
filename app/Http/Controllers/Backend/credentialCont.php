<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\backendBase;
use App\Models\master\user;
use Illuminate\Support\Facades\Auth;
use Session;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;

class credentialCont extends backendBase {
	public function index()
	{
		$status_login = true;
		if(Session::get('status-login') == 'false'){
			$status_login = false;
		}
		$arr_data = array(
					'status_login' => $status_login,
					'title' => $this->helperSite->setPageTitle('Login')
			);
		//$ip_add = Request::server('REMOTE_ADDR');
		//dd($ip_add);

		return view('backend/credential/login', $arr_data);
	}

	public function loginPost(){
		if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password'),'status' => 'Aktif'))){
			$userUp = user::find(Auth::user()->id);
			
            $ip = $userUp->access_ip;
            $checkIP = false;
            if($ip == '*'){
            	$checkIP = true;
            }else{
            	$ip = explode(';', $ip);
            	$access = array_where($ip, function($key, $value) 
					{
						$ip_access = Request::server('REMOTE_ADDR');
						if($ip_access == $value)
					    	return is_string($value);
					});
            	if(count($access)){
            		$checkIP = true;
            	}
            }


            if($checkIP){
	            $userUp->last_login = date('Y-m-d H:i:s');
	            $userUp->login_ip = date('Y-m-d H:i:s');
	            $userUp->update();
	            
	            Session::flash('status-login', 'true');
	            return redirect(route('backDashboard'));
        	}else{
        		Auth::logout();
        		Session::flash('status-login', 'false');
				return redirect(route('backLogin'));
        	}
		}
		Session::flash('status-login', 'false');
		return redirect(route('backLogin'));
	}

	public function logout(){
		Auth::logout();
		return redirect('/');
	}
}