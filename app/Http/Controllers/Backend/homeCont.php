<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\backendBase;
use Session;
use Input;
use DB;

use App\Models\operation\applicant;
use App\Models\operation\applicantStatus;
use App\Models\operation\agent;
use App\Models\operation\paymentAgent;
use App\Http\Controllers\Operation\applicantCont;

class homeCont extends backendBase {
	public function index()
	{
        $this->arr_data['position'] = array(
            'module' => 'dashboard',
            'menu' => 'dashboard',
            'action' => 'view'
        );

        //$this->arr_data['sidebar_status'] = false;

		$this->arr_data['title'] = $this->helperSite->setPageTitle('Dashboard');
		return view('backend/dashboard', $this->arr_data);
	}
}