<?php namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

//helpers
use App\Http\Helpers\site;

abstract class backendBase extends Controller {
	protected $user;
	protected $helperSite;
	protected $arr_data;
	protected $limit;
	
	public function __construct()
	{
		$this->user = Auth::user();
		$this->arr_data['user'] = $this->user;
		$this->helperSite = new Site;
		$this->arr_data['setting'] = $this->helperSite;
		$this->limit = $this->helperSite->getSetting('site_records_per_page');
		$this->arr_data['title'] = $this->helperSite->setPageTitle('Referral');
		$this->arr_data['sidebar_status'] = true;
		$this->arr_data['position'] = array(
            'module' => 'referral',
            'menu' => 'referral',
            'action' => 'view'
        );
	}
}