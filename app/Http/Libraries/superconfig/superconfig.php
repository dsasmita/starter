<?php namespace App\Http\Libraries\superconfig;

use App\Models\master\setting;
use Illuminate\Support\Facades\Auth;

/**
 * Class untuk mendapatkan setting dan menuliskan setting.
 * 
 * @author Rio Astamal <me@rioastamal.net>
 * @version 1.0
 *
 * @author Dadang S <dangs.work@gmail.com>
 * @change log : implement to laravel 5
 */
class SuperConfig {
	private $config = array();
	
	/**
	 * Class constructor.
	 *
	 * Load semua setting disini.
	 */
	public function __construct() {
		// ambil semua setting dari tabel PREFIX_settings
		try {
			$settings = setting::all();
			// fill the config property
			foreach ($settings as $setting) {
				$this->config[$setting->setting_key] = $setting->setting_value;
			}
		} catch (Exception $e) {
		}
	}
	
	public function add_config($config_name, $config_val) {
		$this->config[$config_name] = $config_val;
		
		return $this;
	}
	
	public function get_config($config_name) {
		if (array_key_exists($config_name, $this->config)) {
			return $this->config[$config_name];
		}
		
		return NULL;
	}

	public function update_setting($key = null, $value = null){
		if(!$key OR !$value)
			return false;

		$setting = setting::where('setting_key',$key)->first();

		if(!$setting)
			return $this->create_setting($key,$value);

		$setting->setting_value = $value;
		$setting->save();

		return true;
	}

	public function create_setting($key = null, $value = null){
		if(!$key OR !$value)
			return false;

		$user = Auth::user();

		$setting = new setting();
		$setting->setting_key = $key;
		$setting->setting_value = $value;
		$setting->menu = camel_case($key);
		$setting->created_by = $user->username;
		$setting->updated_by = $user->username;
		$setting->save();

		return true;
	}
}
