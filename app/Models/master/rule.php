<?php namespace App\Models\master;

use Illuminate\Database\Eloquent\Model;

class rule extends Model {

	protected $table = 'rules';

	public function moduleList($module_active){
		$module = $this->where('module','!=','*')->distinct('module')->orderBy('module')->lists('module');
		if(is_array($module_active)){
			foreach ($module_active as $value) {
				if($value == '*'){
					return $module;
				}
			}
			return $module_active;
		}

		return false;
	}

	public function scopeNormal($query)
    {
        return $query->where('id', '!=', 1);
    }

    public function modifiedBy(){
    	return $this->belongsTo('App\models\master\user','updated_by','username');
    }
}