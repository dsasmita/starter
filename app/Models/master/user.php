<?php

namespace App\Models\master;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\models\master\masterRule;
use Auth;

class user extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword,
        SoftDeletes;

    protected $table = 'users';
    //protected $fillable = ['name', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function roles() {
        return $this->belongsToMany('App\Models\master\role', 'role_user');
    }

    public function getRolesList() {
        $roles = $this->roles()->lists('name');
        return $roles;
    }

    public function hakAkses(){
        if ($this->roles()->first()) {
            return $this->roles()->first()->name;
        }
    }

    public function scopeNormal($query) {
        return $query->where('id', '!=', 0);
    }

    public function getModuleList() {
        if ($this->roles()->first()) {
            return $this->roles()->first()->rules()->distinct('module')->orderBy('module')->lists('module');
        }

        return false;
    }

    public function checkAksesModule($module = '') {
        if (!is_null($module)):
            $modules = $this->getModuleList();
            if ($modules) {
                foreach ($modules as $mod) {
                    if ($mod == '*' OR $mod == $module)
                        return true;
                }
            }
        endif;

        return false;
    }

    public function getGroupList($mod = '') {
        if ($this->roles()->first()) {
            return $this->roles()->first()->rules()->where('module', '=', $mod)->orderBy('group')->lists('group');
        }
        return false;
    }

    public function chechAksesGroupModule($mod = '', $group = '') {
        if (Auth::user()->id == 0)
            return true;

        $groups = $this->getGroupList($mod);
        //dd($groups);
        if ($groups AND ! is_null($group)) {
            foreach ($groups as $grp) {
                if ($grp == $group || $grp == '*') {
                    return true;
                }
            }
        }
        return false;
    }

    public function getActionGroup($mod = '', $group = '') {
        if ($this->roles()->first()) {
            return $this->roles()->first()->rules()
                            ->where(function($query) use($mod) {
                                $query->where('module', '=', $mod)
                                ->orWhere('module', '=', '*');
                            })->where(function($query2) use($group) {
                        $query2->where('group', '=', $group)
                                ->orWhere('group', '=', '*');
                    })->orderBy('action')->lists('action');
        }
        return false;
    }

    public function checkActionGroup($mod = '', $group = '', $action = '') {
        $actions = $this->getActionGroup($mod, $group);
        if ($actions AND ! is_null($action)) {
            foreach ($actions as $act) {
                if ($act == '*' OR $act == $action) {
                    return true;
                }
            }
        }
        return false;
    }

}
