<?php namespace App\Models\master;

use Illuminate\Database\Eloquent\Model;

class role extends Model {

	protected $table = 'roles';

	protected $touches = array('rules');

	public function rules()
	{
		return $this->belongsToMany('App\Models\master\rule','role_rule')->withTimestamps();
	}

	public function scopeNormal($query)
    {
        return $query->where('id', '!=', 1);
    }

    public function modifiedBy(){
    	return $this->belongsTo('App\Models\master\user','updated_by','username');
    }
}